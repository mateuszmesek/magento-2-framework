<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magento\Framework\Component;

/**
 * Provides ability to statically register components.
 *
 * @author Josh Di Fabio <joshdifabio@gmail.com>
 *
 * @api
 */
class ComponentRegistrar implements ComponentRegistrarInterface
{
    /**#@+
     * Different types of components
     */
    const MODULE = 'module';
    const LIBRARY = 'library';
    const THEME = 'theme';
    const LANGUAGE = 'language';
    const EDITION = 'edition';
    /**#@- */

    /**#@- */
    private static $paths = [
        self::MODULE => [],
        self::LIBRARY => [],
        self::LANGUAGE => [],
        self::THEME => [],
        self::EDITION => [],
    ];

    const PATH_ROOT = 'root';

    /**
     * Sets the location of a component.
     *
     * @param string $type component type
     * @param string $componentName Fully-qualified component name
     * @param string $path Absolute file path to the component
     * @throws \LogicException
     * @return void
     */
    public static function register($type, $componentName, $path, array $paths = [])
    {
        self::validateType($type);
        if (isset(self::$paths[$type][$componentName])) {
            throw new \LogicException(
                ucfirst($type) . ' \'' . $componentName . '\' from \'' . $path . '\' '
                . 'has been already defined in \'' . self::$paths[$type][$componentName] . '\'.'
            );
        } else {
            $paths[self::PATH_ROOT] = str_replace('\\', '/', $path);

            self::$paths[$type][$componentName] = $paths;
        }
    }

    public function getComponents($type)
    {
        self::validateType($type);

        return array_keys(self::$paths[$type]);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaths($type, string $pathType = self::PATH_ROOT)
    {
        self::validateType($type);

        $paths = [];

        foreach (self::$paths[$type] as $componentName => $path) {
            $paths[$componentName] = $path[$pathType];
        }

        return $paths;
    }

    /**
     * {@inheritdoc}
     */
    public function getPath($type, $componentName, string $pathType = self::PATH_ROOT)
    {
        self::validateType($type);
        return isset(self::$paths[$type][$componentName][$pathType]) ? self::$paths[$type][$componentName][$pathType] : null;
    }

    /**
     * Checks if type of component is valid
     *
     * @param string $type
     * @return void
     * @throws \LogicException
     */
    private static function validateType($type)
    {
        if (!isset(self::$paths[$type])) {
            throw new \LogicException('\'' . $type . '\' is not a valid component type');
        }
    }
}
